#  Copyright 2021-2023 Glassdoor, Inc.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import argparse

import docker

parser = argparse.ArgumentParser()
parser.add_argument("--imageDir", required=True, help="Directory with docker image files")
args = parser.parse_args()

client = docker.from_env()
client.images.build(path=args.imageDir, tag='mlcc-test')
print(client.containers.run(image='mlcc-test', auto_remove=True).decode("utf-8"))
