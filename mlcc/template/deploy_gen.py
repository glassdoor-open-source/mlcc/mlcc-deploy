#  Copyright 2021-2023 Glassdoor, Inc.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import argparse
import importlib
import logging
import os
import shutil
import tempfile

from mlcc.common import ConfigUtil


def generate(project_variant, project_version, deploy_config, task_templates, output_dir, compress=True):
    project_config = ConfigUtil.load_project_config()
    project_name = ConfigUtil.get_config_value(config=project_config, path=['name'])
    project_variant = project_variant
    project_version = project_version

    deploy_config.update({'projectName': project_name, 'projectVariant': project_variant,
                          'projectVersion': project_version})

    os.makedirs(output_dir, exist_ok=True)

    for task_name in project_config['tasks']:
        task_config = project_config['tasks'][task_name]
        task_type = task_config['type']
        template_out_dir = os.path.join(output_dir, f'task_{task_name}')
        temp_dir = None
        if compress:
            temp_dir = tempfile.TemporaryDirectory()
            template_out_dir = temp_dir.name
        os.makedirs(template_out_dir, exist_ok=True)
        if task_type not in taskTemplates:
            raise Exception(f'Unable to generate deployment files for task {task_name}: No template registered for '
                            f'task type {task_type}')
        task_templates[task_type].generate(project_config=project_config, project_name=project_name,
                                           project_variant=project_variant, project_version=project_version,
                                           task_name=task_name, deploy_config=deploy_config,
                                           output_dir=template_out_dir)
        archive_filename = os.path.join(output_dir, f'task_{task_name}')
        if compress:
            shutil.make_archive(archive_filename, 'zip', template_out_dir)
            temp_dir.cleanup()


if __name__ == '__main__':
    # Initialize parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--projectVariant", required=True, help="Project variant")
    parser.add_argument("--projectVersion", required=True, help="Project variant")
    parser.add_argument("-c", "--deployConfig", help="Deploy YAML config")
    parser.add_argument("-o", "--outputDir", required=True, help="Output directory for deployment files")
    parser.add_argument("--compress", action='store_true', help="Compress the files used to build the images")
    parser.add_argument('--logLevel', default='warning',
                        help='Provide logging level. Example --loglevel debug, default=warning')

    args = parser.parse_args()

    logging.basicConfig(level=args.logLevel.upper())

    shutil.rmtree(args.outputDir, ignore_errors=True)
    taskTemplates = {'python': importlib.import_module("mlcc.template.task.python.deploy_gen"),
                     'mlcc-pipeline': importlib.import_module("mlcc.template.task.mlcc_pipeline.deploy_gen")}
    generate(args.projectVariant, args.projectVersion, ConfigUtil.load_yaml_file(args.deployConfig),
             taskTemplates, args.outputDir, compress=args.compress)
