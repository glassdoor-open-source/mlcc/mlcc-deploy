#  Copyright 2021-2023 Glassdoor, Inc.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
import pkgutil
import yaml
import shutil
from pathlib import Path
from string import Template
import logging
import re

from pprint import pprint
from mlcc.common import ConfigUtil

docker_filename = "Dockerfile"
default_config_filename = "base-config.yaml"


def load_resource(name):
    return pkgutil.get_data("mlcc.template.task.base_python", name).decode("utf-8")


def generate(project_config, project_name, project_variant, project_version, task_name, deploy_config,
             output_dir, default_deploy_config = None, additional_conda_config = None, docker_resource = None):
    deploy_config = deploy_config.copy()
    if default_deploy_config == None:
        default_deploy_config = yaml.safe_load(load_resource(default_config_filename))
    deploy_config = ConfigUtil.get_config_with_defaults(config=deploy_config, defaults=default_deploy_config)

    task_deploy_config = ConfigUtil.get_config_value(config=project_config, path=['tasks', task_name, 'deploy'], default={})
    deploy_config = ConfigUtil.get_config_with_defaults(config=task_deploy_config, defaults=deploy_config, merge_collections=True)

    task_run_config = ConfigUtil.get_config_value(config=project_config, path=['tasks', task_name, 'run'], default={})
    if 'script' not in task_run_config:
        task_run_config['script'] = task_name + ".py"
    deploy_config['run'] = ConfigUtil.get_config_with_defaults(config=task_run_config, defaults=deploy_config.get('run'))
    script = deploy_config['run']['script']
    if 'entryPoint' not in deploy_config:
        deploy_config['entryPoint'] = f'ENTRYPOINT ["conda", "run", "--no-capture-output", "-n", "base", "python", "{script}"]'
    elif isinstance(deploy_config['entryPoint'], list):
        deploy_config['entryPoint'] = f'ENTRYPOINT [' + ", ".join(f'"{x}"' for x in deploy_config['entryPoint']) + ']'

    # Env commands
    env_commands = deploy_config.get('env', [])
    deploy_config['envCommands'] = "\n".join(env_commands)

    # Pip commands
    pip_commands = []
    for pipPackage in deploy_config.get('pip', []):
        pip_commands.append(f'RUN pip install {pipPackage} && pip cache purge')
    deploy_config['pipCommands'] = "\n".join(pip_commands)

    # Additional commands
    additional_commands = deploy_config.get('additionalCommands', '')
    if isinstance(additional_commands, list):
        additional_commands = "\n".join(additional_commands)
    deploy_config['additionalCommands'] = additional_commands

    # Copy commands
    copy_commands = [f'COPY {script} .']
    for file_to_copy in deploy_config.get('copyFiles', []):
        if file_to_copy is None:
            raise Exception("Missing filename in copyFiles section")
        copy_commands.append(f'COPY {file_to_copy} ./{file_to_copy}')
        dest_filename = os.path.join(output_dir, file_to_copy)
        os.makedirs(Path(dest_filename).parent, exist_ok=True)
        shutil.copyfile(file_to_copy, dest_filename)

    for dir_to_copy in deploy_config.get('copyDirectories', []):
        copy_commands.append(f'COPY {dir_to_copy} ./{dir_to_copy}')
        dest_dir = os.path.join(output_dir, dir_to_copy)
        os.makedirs(dest_dir, exist_ok=True)
        shutil.copytree(dir_to_copy, dest_dir, dirs_exist_ok=True)

    # Pip config
    pip_config_filename = deploy_config.get('pipConfig')
    deploy_config['copyPipConfig'] = ''
    if pip_config_filename is not None:
        deploy_config['copyPipConfig'] = f'COPY {pip_config_filename} /etc/pip.conf'
        dest_pip_config_filename = os.path.join(output_dir, 'pip.conf')
        shutil.copyfile(pip_config_filename, dest_pip_config_filename)

    deploy_config['copyCommands'] = '\n'.join(copy_commands)

    logging.info(f'Generating deployment files for task {task_name} using deployConfig={deploy_config}')

    # Copy the run script to the output directory
    shutil.copyfile(deploy_config['run']['script'], os.path.join(output_dir, deploy_config['run']['script']))

    # TODO Copy the conda yaml file to the output directory
    conda_filename = deploy_config['condaFile']
    output_conda_filename = os.path.join(output_dir, conda_filename)
    if additional_conda_config:
        with open(conda_filename) as f:
            conda_config = yaml.safe_load(f.read())
        conda_config = ConfigUtil.get_config_with_defaults(config=conda_config, defaults=additional_conda_config, merge_collections=True)
        with open(output_conda_filename, 'w') as dest_conda_file:
            yaml.dump(conda_config, dest_conda_file, default_flow_style=False)
    else:
        shutil.copyfile(conda_filename, output_conda_filename)

    # Generate the Dockerfile and write it to the output directory
    if not docker_resource:
        docker_resource = load_resource(docker_filename)
    dockerfile = Template(docker_resource).substitute(**deploy_config)
    dockerfile = re.sub(r'\n\s*\n', '\n', dockerfile, re.MULTILINE)
    docker_output_path = os.path.join(output_dir, docker_filename)
    with open(docker_output_path, "w") as docker_output_file:
        docker_output_file.write(dockerfile)
