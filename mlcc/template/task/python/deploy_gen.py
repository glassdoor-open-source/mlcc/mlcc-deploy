#  Copyright 2021-2023 Glassdoor, Inc.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import pkgutil
import yaml

from mlcc.template.task.base_python.deploy_gen import generate as base_python_deploy_generate

docker_filename = "Dockerfile"
default_config_filename = "default-config.yaml"


def load_resource(name):
    return pkgutil.get_data("mlcc.template.task.python", name).decode("utf-8")


def generate(project_config, project_name, project_variant, project_version, task_name, deploy_config, output_dir):

    default_deploy_config = yaml.safe_load(load_resource(default_config_filename))

    base_python_deploy_generate(
        project_config=project_config,
        project_name=project_name,
        project_variant=project_variant,
        project_version=project_version,
        task_name=task_name,
        deploy_config=deploy_config,
        output_dir=output_dir,
        default_deploy_config=default_deploy_config,
    )
